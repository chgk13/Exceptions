from contextlib import ContextDecorator
import sys


class err_redirect:
    def __init__(self, dest=None):
        print(dest)
        self.dest = dest
        self.dest_file = None

    def __enter__(self):
        self._saved_stderr = sys.stderr
        if self.dest:
            self.dest_file = open(self.dest, 'w')
            sys.stderr = self.dest_file
        else:
            sys.stderr = sys.stdout

    def __exit__(self, exc_class, exc_type, tb):
        sys.stderr = self._saved_stderr
        if self.dest_file:
            self.dest_file.close()


class stderr_redirect(err_redirect, ContextDecorator):
    pass


@stderr_redirect(dest=None)
def func1(*args, **kwargs):
    print(sys.stderr)
    print('123', file=sys.stderr)
    print(0/1)


@stderr_redirect(dest='/home/chgk13/python/epam/Exceptions/file2.txt')
def func2(*args, **kwargs):
    print(sys.stderr)
    print('213', file=sys.stderr)


def test():
    func1()
    func2()


if __name__ == '__main__':
    test()
