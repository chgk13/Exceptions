import os


class pidfile:
    def __init__(self, file_name: str):
        self.file_name = file_name
        self.pid = None

    def __enter__(self):
        self.path = os.path.join(os.path.abspath(
            os.path.dirname(self.file_name)), self.file_name)
        try:
            self.pid = open(self.path, 'r')
            first_chr = self.pid.readline()[0]
            if first_chr == '1':
                raise Exception('Code is already running')
        except IOError:
            pass
        except IndexError:
            pass
        finally:
            if self.pid:
                self.pid.close()
        self.pid = open(self.path, 'w')
        self.pid.write('1')
        self.pid.close()

    def __exit__(self, *exc_info):
        self.pid = open(self.path, 'w')
        self.pid.close()
