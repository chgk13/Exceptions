from hw_exceptions import pidfile
import time


with pidfile('file1.txt'):
    print(1)
    time.sleep(10)
    print(2)
